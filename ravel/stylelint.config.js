// stylelint.config.js
const prettierConfig = require('./prettier.config.js');

// Transform from snake-case to TitleCase
const titleCase = (str) =>
    str.replace(/(-|^)([^-]?)/g, (match, _, char) => char.toUpperCase());

// The regex must match CSS selectors conforming to the BEM naming conventions
// you want to enforce.
const customBemSelector = () => {
    const block = '[A-Z][a-z]+(?:[A-Z][a-z]+)*';
    const kebabCase = '[a-z][a-zA-Z0-9]*';
    const element = `(?:_${kebabCase})?`;
    const modifier = `(?:___${kebabCase})?`;
    const attribute = '(?:\\[.+\\])?';

    return new RegExp(`^\\.${block}${element}${modifier}${attribute}$`);
};

module.exports = {
    extends: [
        'stylelint-config-standard',
        'stylelint-config-rational-order',
        'stylelint-config-airbnb',
        'stylelint-config-css-modules',
        'stylelint-config-prettier',
    ],
    plugins: [
        'stylelint-prettier',
        'stylelint-selector-bem-pattern',
        'stylelint-scss',
        'stylelint-group-selectors',
        'stylelint-use-nesting',
        'stylelint-no-unsupported-browser-features',
    ],
    rules: {
        'at-rule-no-unknown': null,
        'scss/at-rule-no-unknown': null,
        'plugin/no-unsupported-browser-features': [
            true,
            {
                severity: 'warning',
            },
        ],
        'csstools/use-nesting': true,
        'plugin/stylelint-group-selectors': null,
        'scss/selector-no-redundant-nesting-selector': true,
        'scss/selector-nest-combinators': 'always',
        'scss/no-duplicate-dollar-variables': true,
        'scss/no-duplicate-mixins': true,
        'scss/operator-no-unspaced': true,
        'scss/operator-no-newline-before': true,
        'scss/operator-no-newline-after': true,
        'scss/map-keys-quotes': 'always',
        'scss/dimension-no-non-numeric-values': true,
        'scss/comment-no-loud': true,
        'scss/dollar-variable-colon-space-after': 'always',
        'scss/dollar-variable-colon-space-before': 'never',
        'prettier/prettier': [true, prettierConfig],
        'plugin/selector-bem-pattern': {
            // Derive component names from the file name
            implicitComponents: true,
            // Use the default BEM preset
            preset: 'bem',
            // Configures the valid selectors
            componentSelectors: {
                initial: customBemSelector,
            },
            // We allow any custom property (CSS var) names
            ignoreCustomProperties: '.*',
        },
        'block-no-empty': null,
        'rule-empty-line-before': [
            'always',
            {
                except: ['first-nested', 'after-single-line-comment'],
            },
        ],
    },
};
