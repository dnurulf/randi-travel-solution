/* eslint-disable @next/next/no-img-element */
import React from 'react';

export const HeaderImage = (): JSX.Element => {
    return (
        <div className="relative">
            <img src="/HeaderImage.png" alt="Header Image" className="w-full" />
            <div className="absolute z-10 w-full bottom-0 h-full bg-black bg-opacity-30">
                <div className="h-full flex flex-col justify-end pb-2 pl-24">
                    <p className="text-3xl text-white font-bold">Ravel</p>
                    <p className="text-xl text-white font-bold">
                        Travel Solution
                    </p>
                </div>
            </div>
        </div>
    );
};

export default HeaderImage;
