/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { HeaderImage } from './HeaderImage.component';

const meta: Meta<typeof HeaderImage> = {
    component: HeaderImage,
};

export default meta;
type Story = StoryObj<typeof HeaderImage>;

const HeaderImageWithHooks = () => {
    const { control } = useForm();

    return <HeaderImage />;
};

export const Example: Story = {
    render: () => <HeaderImageWithHooks />,
};
