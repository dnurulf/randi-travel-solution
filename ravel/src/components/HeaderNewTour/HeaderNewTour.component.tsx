/* eslint-disable @next/next/no-img-element */
import React from 'react';

import { IPropTypes } from './HeaderNewTour.types';

export const HeaderNewTour = (props: IPropTypes): JSX.Element => {
    const { title, subtitle, image, children } = props;

    return (
        <div className="sectionItem">
            <div
                className="sectionItem__image shadow-lg"
                style={{
                    backgroundImage: `url(${image})`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                }}
            ></div>

            <div className="sectionItem__body">
                <div className="sectionItem__cap">
                    <p className="title" data-cy="top-tour-name">
                        {title}
                    </p>
                    <p className="subtitle">{subtitle}</p>
                </div>

                <div>{children}</div>
            </div>
        </div>
    );
};

export default HeaderNewTour;
