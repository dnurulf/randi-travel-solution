/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';

import { HeaderNewTour } from './HeaderNewTour.component';

const meta: Meta<typeof HeaderNewTour> = {
    component: HeaderNewTour,
};

export default meta;
type Story = StoryObj<typeof HeaderNewTour>;

const HeaderNewTourWithHooks = () => {
    return (
        <HeaderNewTour
            title="Tujuan baru"
            subtitle="Maldives Surganya Dunia"
            image="https://source.unsplash.com/600x400/?coat"
        >
            Ini adalah body dari section item
        </HeaderNewTour>
    );
};

export const Example: Story = {
    render: () => <HeaderNewTourWithHooks />,
};
