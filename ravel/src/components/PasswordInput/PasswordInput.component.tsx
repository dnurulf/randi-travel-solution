import React, { useState } from 'react';
import { useController } from 'react-hook-form';
import { IoIosEye, IoIosEyeOff } from 'react-icons/io';

import { IPropTypes } from './PasswordInput.types';

export const PasswordInput = ({
    label,
    errors,
    ...props
}: IPropTypes): JSX.Element => {
    const { field } = useController(props);
    const [isPasswordShown, setIsPasswordShown] = useState(false);

    return (
        <div className="formControl">
            <label className="formControl__label">{label}</label>
            <div className="flex flex-row items-center">
                <div className="flex-1">
                    <input
                        className={`formControl__input border-r-0 rounded-e-none ${errors && '!border-red-500'}`}
                        type={isPasswordShown ? 'text' : 'password'}
                        {...field}
                        {...props}
                    />
                </div>
                <div
                    className={`p-3 border border-l-0 rounded-e border-font-300 ${errors && '!border-red-500'}`}
                >
                    {isPasswordShown ? (
                        <IoIosEyeOff
                            size={24}
                            onClick={() => setIsPasswordShown(!isPasswordShown)}
                        />
                    ) : (
                        <IoIosEye
                            size={24}
                            onClick={() => setIsPasswordShown(!isPasswordShown)}
                        />
                    )}
                </div>
            </div>
            {errors && <span className="formControl__error">{errors}</span>}
        </div>
    );
};

export default PasswordInput;
