import { UseControllerProps } from 'react-hook-form';

export interface IPropTypes extends UseControllerProps<any> {
    label: string;
    placeholder?: string;
    errors?: string;
    onChange?: () => void;
}
