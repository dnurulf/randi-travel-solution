/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { PasswordInput } from './PasswordInput.component';

const meta: Meta<typeof PasswordInput> = {
    component: PasswordInput,
};

export default meta;
type Story = StoryObj<typeof PasswordInput>;

const PasswordInputWithHooks = () => {
    const { control } = useForm();

    return (
        <PasswordInput
            control={control}
            name="password"
            placeholder="Enter your Password"
            label="Password"
            rules={{ required: true }}
            errors=""
            onChange={() => {}}
        />
    );
};

export const Example: Story = {
    render: () => <PasswordInputWithHooks />,
};
