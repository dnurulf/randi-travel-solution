/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';

import { IPropTypes } from './Dropdown.types';

export const Dropdown = ({
    options,
    onSelect,
    userName,
}: IPropTypes): JSX.Element => {
    const [isOpen, setIsOpen] = useState(false);

    const handleToggleDropdown = () => {
        setIsOpen(!isOpen);
    };

    const handleSelectOption = () => {
        setIsOpen(false);
        onSelect();
    };

    return (
        <div className="dropdown">
            <div
                className="dropdown__header flex flex-row items-center gap-2"
                data-cy="h-profile"
                onClick={handleToggleDropdown}
            >
                <p className="hidden lg:inline-blok">Halo, {userName}</p>
                <img
                    src="/profileImage.png"
                    alt="User's Profile Image"
                    width={30}
                    height={30}
                />
                <span className="dropdown__arrow">{isOpen ? '▲' : '▼'}</span>
            </div>
            {isOpen && (
                <div className="dropdown__options">
                    {options.map((option) => (
                        <div
                            data-cy={option.dataName}
                            key={option.value}
                            className="dropdown__option bg-white"
                            onClick={handleSelectOption}
                        >
                            {option.label}
                        </div>
                    ))}
                </div>
            )}
        </div>
    );
};

export default Dropdown;
