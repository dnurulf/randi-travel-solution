import { IDropdownOption } from '@/types';

export interface IPropTypes {
    options: IDropdownOption[];
    userName: string;
    onSelect: () => void;
}
