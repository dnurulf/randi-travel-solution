/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';

import { profileDropdownOption } from '@/constants';

import { Dropdown } from './Dropdown.component';

const meta: Meta<typeof Dropdown> = {
    component: Dropdown,
};

export default meta;
type Story = StoryObj<typeof Dropdown>;

const DropdownWithHooks = () => {
    return (
        <Dropdown
            userName="Dwiyan"
            options={profileDropdownOption}
            onSelect={() => {}}
        />
    );
};

export const Example: Story = {
    render: () => <DropdownWithHooks />,
};
