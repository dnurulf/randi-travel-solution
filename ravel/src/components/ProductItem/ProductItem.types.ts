export interface IPropTypes {
    title: JSX.Element | string;
    description: string;
    image: string;
    price: number;
    index: number;
}
