/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';

import { ProductItem } from './ProductItem.component';

const meta: Meta<typeof ProductItem> = {
    component: ProductItem,
};

export default meta;
type Story = StoryObj<typeof ProductItem>;

const ProductItemWithHooks = () => {
    return (
        <div className="flex flex-col gap-10">
            <ProductItem
                index={0}
                title="Tujuan baru"
                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit skhjkjhsdkjfhskjdfskjdfh"
                image="https://source.unsplash.com/250x200/?coat"
                price={1000000}
            />
        </div>
    );
};

export const Example: Story = {
    render: () => <ProductItemWithHooks />,
};
