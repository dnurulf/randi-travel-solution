/* eslint-disable @next/next/no-img-element */
import React from 'react';

import { formatPrice } from '@/utils';

import { IPropTypes } from './ProductItem.types';

export const ProductItem = (props: IPropTypes): JSX.Element => {
    const { title, image, description, price, index } = props;

    return (
        <div className="productItem productItem--horizontal items-center">
            <div className="productItem__image">
                <img
                    src={image}
                    alt={description}
                    data-cy={`tour-image-${index}`}
                />
            </div>

            <div className="productItem__body gap-6">
                <div className="flex-1">
                    <p className="title">{title}</p>
                </div>
                <div className="flex-1">
                    <p className="description" data-cy={`tour-desc-${index}`}>
                        {description}
                    </p>
                </div>
                <div className="flex-1">
                    <p
                        className="productItem__price"
                        data-cy={`tour-price-${index}`}
                    >
                        Mulai dari: {formatPrice(price)}
                    </p>
                </div>
            </div>
        </div>
    );
};

export default ProductItem;
