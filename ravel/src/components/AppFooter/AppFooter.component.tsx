/* eslint-disable @next/next/no-img-element */
import React from 'react';

export const AppFooter = (): JSX.Element => {
    return (
        <div className="appFooter" data-cy="f-ravel">
            <div className="appFooter__logo">
                <img
                    src="/FooterLogo.png"
                    alt="Footer Logo"
                    width={250}
                    height={75}
                />
            </div>
            <div className="flex-1">
                <p>Copyright ©2023 All rights reserved</p>
            </div>
        </div>
    );
};

export default AppFooter;
