/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { AppFooter } from './AppFooter.component';

const meta: Meta<typeof AppFooter> = {
    component: AppFooter,
};

export default meta;
type Story = StoryObj<typeof AppFooter>;

const AppFooterWithHooks = () => {
    const { control } = useForm();

    return <AppFooter />;
};

export const Example: Story = {
    render: () => <AppFooterWithHooks />,
};
