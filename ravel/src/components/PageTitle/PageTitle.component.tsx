import React from 'react';

import { IPropTypes } from './PageTitle.types';

export const PageTitle = ({
    title,
    subtitle,
    dataName,
}: IPropTypes): JSX.Element => {
    return (
        <div className="pageTitle">
            <p className="text-xl lg:text-5xl font-bold" data-cy={dataName}>
                {title}
            </p>
            {subtitle && (
                <p className="text-xl lg:text-5xl font-semibold">{subtitle}</p>
            )}
        </div>
    );
};

export default PageTitle;
