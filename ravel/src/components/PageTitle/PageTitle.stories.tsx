/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { PageTitle } from './PageTitle.component';

const meta: Meta<typeof PageTitle> = {
    component: PageTitle,
};

export default meta;
type Story = StoryObj<typeof PageTitle>;

const PageTitleWithHooks = () => {
    const { control } = useForm();

    return <PageTitle title="Title" subtitle="subtitle" dataName="test" />;
};

export const Example: Story = {
    render: () => <PageTitleWithHooks />,
};
