export interface IPropTypes {
    title: string | JSX.Element;
    subtitle?: string;
    dataName: string;
}
