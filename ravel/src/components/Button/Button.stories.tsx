/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { Button } from './Button.component';

const meta: Meta<typeof Button> = {
    component: Button,
};

export default meta;
type Story = StoryObj<typeof Button>;

const ButtonWithHooks = () => {
    const { control } = useForm();

    return <Button>Text</Button>;
};

export const Example: Story = {
    render: () => <ButtonWithHooks />,
};
