export interface IPropTypes {
    children: JSX.Element | string;
    isLoading?: boolean;
    type?: 'submit' | 'reset' | 'button' | undefined;

    onClick?: () => void;
}
