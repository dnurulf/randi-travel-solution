import React from 'react';

import { IPropTypes } from './Button.types';

export const Button = ({
    children,
    type = 'submit',
    isLoading = false,
    ...props
}: IPropTypes): JSX.Element => {
    return (
        <button
            className="button button--primary"
            disabled={isLoading}
            type={type}
            {...props}
        >
            {isLoading ? 'Loading' : children}
        </button>
    );
};

export default Button;
