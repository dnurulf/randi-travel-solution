export interface IPropTypes {
    children: JSX.Element | string;
    width?: string;
}
