import React from 'react';

import { IPropTypes } from './Card.types';

export const Card = ({ children, width }: IPropTypes): JSX.Element => {
    return (
        <div className={`card bg-white ${width && `w-[${width}px]`}`}>
            {children}
        </div>
    );
};

export default Card;
