/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { Card } from './Card.component';

const meta: Meta<typeof Card> = {
    component: Card,
};

export default meta;
type Story = StoryObj<typeof Card>;

const CardWithHooks = () => {
    const { control } = useForm();

    return <Card>Hello this is card</Card>;
};

export const Example: Story = {
    render: () => <CardWithHooks />,
};
