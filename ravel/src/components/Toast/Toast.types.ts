export interface IPropTypes {
    message: string;
    type: 'error' | 'success';
}
