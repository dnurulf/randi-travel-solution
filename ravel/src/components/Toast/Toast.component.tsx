import React, { useState, useEffect } from 'react';

import { IPropTypes } from './Toast.types';

export const Toast = ({ message, type = 'error' }: IPropTypes) => {
    const [show, setShow] = useState(false);

    useEffect(() => {
        setShow(true);
        const timer = setTimeout(() => setShow(false), 3000);
        return () => clearTimeout(timer);
    }, []);

    return (
        <div
            className={`fixed text-xl font-bold top-5 right-0 transform -translate-x-10 ${type === 'error' ? 'bg-red-500' : 'bg-green-500'} text-white px-10 py-6 rounded-md transition-opacity duration-500 ${show ? 'opacity-100' : 'opacity-0'}`}
        >
            {message}
        </div>
    );
};

export default Toast;
