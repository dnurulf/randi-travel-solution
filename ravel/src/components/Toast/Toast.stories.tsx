/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { Toast } from './Toast.component';

const meta: Meta<typeof Toast> = {
    component: Toast,
};

export default meta;
type Story = StoryObj<typeof Toast>;

const ToastWithHooks = () => {
    const { control } = useForm();

    return <Toast message="Hello this is Toast" type="error" />;
};

export const Example: Story = {
    render: () => <ToastWithHooks />,
};
