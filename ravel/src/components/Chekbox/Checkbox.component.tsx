import React from 'react';
import { useController } from 'react-hook-form';

import { IPropTypes } from './Checkbox.types';

export const Checkbox = ({
    label,
    errors,
    ...props
}: IPropTypes): JSX.Element => {
    const { field } = useController(props);

    return (
        <div className="flex flex-row gap-2">
            <input {...field} {...props} type="checkbox"></input>
            <label className="formControl__label" htmlFor={props.name}>
                {label}
            </label>
            {errors && <span className="formControl__error">{errors}</span>}
        </div>
    );
};

export default Checkbox;
