/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { Checkbox } from './Checkbox.component';

const meta: Meta<typeof Checkbox> = {
    component: Checkbox,
};

export default meta;
type Story = StoryObj<typeof Checkbox>;

const CheckboxWithHooks = () => {
    const { control } = useForm();

    return (
        <Checkbox
            control={control}
            name="FirstName"
            placeholder="Enter your First Name"
            label="First Name"
            rules={{ required: true }}
            errors=""
            onChange={() => {}}
        />
    );
};

export const Example: Story = {
    render: () => <CheckboxWithHooks />,
};
