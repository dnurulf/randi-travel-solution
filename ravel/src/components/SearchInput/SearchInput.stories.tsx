/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { SearchInput } from './SearchInput.component';

const meta: Meta<typeof SearchInput> = {
    component: SearchInput,
};

export default meta;
type Story = StoryObj<typeof SearchInput>;

const SearchInputWithHooks = () => {
    return <SearchInput onChange={() => {}} />;
};

export const Example: Story = {
    render: () => <SearchInputWithHooks />,
};
