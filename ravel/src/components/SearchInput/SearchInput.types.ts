export interface IPropTypes {
    onChange: (value: string) => void;
}
