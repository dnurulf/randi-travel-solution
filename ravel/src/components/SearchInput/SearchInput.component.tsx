import React from 'react';
import { FaSearch } from 'react-icons/fa';

import { IPropTypes } from './SearchInput.types';

export const SearchInput = (props: IPropTypes): JSX.Element => {
    const { onChange } = props;

    const onSearchInput = (event: React.ChangeEvent<HTMLInputElement>) => {
        onChange(event.target.value);
    };

    return (
        <div className="formControl">
            <div className="flex flex-row items-center">
                <div className="flex-1">
                    <input
                        placeholder="Cari Tempat"
                        className="formControl__input border-r-0 rounded-e-none"
                        onChange={onSearchInput}
                    />
                </div>
                <div className="p-3 border border-l-0 rounded-e border-font-300">
                    <FaSearch size={24} />
                </div>
            </div>
        </div>
    );
};

export default SearchInput;
