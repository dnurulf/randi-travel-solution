/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';
import { useForm } from 'react-hook-form';

import { Input } from './Input.component';

const meta: Meta<typeof Input> = {
    component: Input,
};

export default meta;
type Story = StoryObj<typeof Input>;

const InputWithHooks = () => {
    const { control } = useForm();

    return (
        <Input
            control={control}
            name="FirstName"
            placeholder="Enter your First Name"
            label="First Name"
            rules={{ required: true }}
            errors=""
        />
    );
};

export const Example: Story = {
    render: () => <InputWithHooks />,
};
