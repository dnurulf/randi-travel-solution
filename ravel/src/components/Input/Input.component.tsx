import React from 'react';
import { useController } from 'react-hook-form';

import { IPropTypes } from './Input.types';

export const Input = ({ label, errors, ...props }: IPropTypes): JSX.Element => {
    const { field } = useController(props);

    return (
        <div className="formControl">
            <label className="formControl__label">{label}</label>
            <input
                className={`formControl__input ${errors && '!border-red-500'}`}
                {...field}
                {...props}
            />
            {errors && <span className="formControl__error">{errors}</span>}
        </div>
    );
};

export default Input;
