/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';

import { ProductCard } from './ProductCard.component';

const meta: Meta<typeof ProductCard> = {
    component: ProductCard,
};

export default meta;
type Story = StoryObj<typeof ProductCard>;

const ProductCardWithHooks = () => {
    return (
        <div className="flex flex-row gap-10">
            <ProductCard
                title="Tujuan baru"
                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit skhjkjhsdkjfhskjdfskjdfh"
                image="https://source.unsplash.com/160x160/?coat"
            />

            <ProductCard
                title="Tujuan baru"
                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit skhjkjhsdkjfhskjdfskjdfh"
                image="https://source.unsplash.com/160x160/?coat"
            />

            <ProductCard
                title="Tujuan baru"
                description="Lorem ipsum dolor sit amet, consectetur adipiscing elit skhjkjhsdkjfhskjdfskjdfh"
                image="https://source.unsplash.com/160x160/?coat"
            />
        </div>
    );
};

export const Example: Story = {
    render: () => <ProductCardWithHooks />,
};
