/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Image from 'next/image';

import { truncate } from '@/utils';

import { IPropTypes } from './ProductCard.types';

export const ProductCard = (props: IPropTypes): JSX.Element => {
    const { title, image, description } = props;

    return (
        <div className="productCard">
            <div
                className="productCard__image"
                style={{
                    backgroundImage: `url(${image})`,
                    backgroundRepeat: 'no-repeat',
                    backgroundSize: 'cover',
                    backgroundPosition: 'center',
                }}
            ></div>

            <div className="productCard__body">
                <p className="title">{title}</p>
                <p className="description">{truncate(description, 50)}</p>
            </div>
        </div>
    );
};

export default ProductCard;
