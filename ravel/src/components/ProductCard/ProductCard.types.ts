export interface IPropTypes {
    title: string;
    image: string;
    description: string;
}
