/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';

import { menus } from '@/constants';

import { AppHeader } from './AppHeader.component';

const meta: Meta<typeof AppHeader> = {
    component: AppHeader,
};

export default meta;
type Story = StoryObj<typeof AppHeader>;

const AppHeaderWithHooks = () => {
    return <AppHeader menus={menus} />;
};

export const Example: Story = {
    render: () => <AppHeaderWithHooks />,
};
