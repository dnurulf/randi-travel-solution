import { IMenu } from '@/types';

export interface IPropTypes {
    menus: IMenu[];
}
