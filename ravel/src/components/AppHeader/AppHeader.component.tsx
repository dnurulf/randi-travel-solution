/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { usePathname } from 'next/navigation';

import { Dropdown } from '@/components';
import { profileDropdownOption } from '@/constants';
import { useAppSelector } from '@/redux/hooks';

import { IPropTypes } from './AppHeader.types';

export const AppHeader = ({ menus }: IPropTypes): JSX.Element => {
    // hooks
    const { user } = useAppSelector((state) => state.session);
    const router = useRouter();
    const pathname = usePathname();

    // methods
    const handleSelect = () => {
        localStorage.removeItem('authToken');
        localStorage.removeItem('userInfo');

        router.push('/login');
    };

    return (
        <div className="header">
            <div className="header__logo">
                <Link href="/">
                    <img src="/Logo.png" data-cy="h-logo" alt="Logo" />
                </Link>
            </div>
            <div className="nav">
                <ul className="nav__menu">
                    {menus.map((item, index) => (
                        <li
                            className="nav__item"
                            key={index}
                            data-cy={item.dataName}
                        >
                            <Link
                                href={item.to}
                                className={
                                    pathname === item.to ? 'text-green-500' : ''
                                }
                            >
                                {item.label}
                            </Link>
                        </li>
                    ))}
                </ul>
            </div>
            <div className="header__profile">
                <Dropdown
                    userName={user.name}
                    options={profileDropdownOption}
                    onSelect={handleSelect}
                />
            </div>
        </div>
    );
};

export default AppHeader;
