export interface IPropTypes {
    title: string | JSX.Element;
    image: string;
    children: JSX.Element | string;
    subtitle: string | JSX.Element;
    onCallToActionClick: () => void;
}
