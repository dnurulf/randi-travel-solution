/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';

import { TestimonySection } from './TestimonySection.component';

const meta: Meta<typeof TestimonySection> = {
    component: TestimonySection,
};

export default meta;
type Story = StoryObj<typeof TestimonySection>;

const TestimonySectionWithHooks = () => {
    return (
        <TestimonySection
            title="Tujuan baru"
            subtitle="Maldives Surganya Dunia"
            image="https://source.unsplash.com/600x400/?coat"
            onCallToActionClick={() => {}}
        >
            Ini adalah body dari section item
        </TestimonySection>
    );
};

export const Example: Story = {
    render: () => <TestimonySectionWithHooks />,
};
