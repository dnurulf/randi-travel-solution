/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { FaArrowRight } from 'react-icons/fa6';

import { Button } from '@/components/Button/Button.component';

import { IPropTypes } from './TestimonySection.types';

export const TestimonySection = (props: IPropTypes): JSX.Element => {
    const { title, subtitle, image, children, onCallToActionClick } = props;

    return (
        <div className="sectionItem">
            <div className="sectionItem__image">
                <img
                    className="object-cover object-center h-full"
                    src={image}
                    alt={String(title)}
                />
            </div>
            <div className="sectionItem__body">
                <div className="sectionItem__cap">
                    <p className="title" data-cy="top-tour-name">
                        {title}
                    </p>
                    <p className="subtitle">{subtitle}</p>
                </div>

                {children}

                <div className="sectionItem__footer">
                    <div>
                        <Button
                            data-cy="btn-testimoni"
                            onClick={() => onCallToActionClick()}
                        >
                            <div className="flex flex-row items-center gap-2">
                                Testimoni
                                <FaArrowRight />
                            </div>
                        </Button>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default TestimonySection;
