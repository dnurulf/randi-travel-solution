/* eslint-disable @next/next/no-img-element */
import React from 'react';
import { FaStar } from 'react-icons/fa';

import { getFirstCharacters } from '@/utils';

import { IPropTypes } from './TestimonyCard.types';

export const TestimonyCard = (props: IPropTypes): JSX.Element => {
    const { name, comment, profileImage, rate, index } = props;

    const renderRate = () => {
        const content = [];
        const ratings = Math.floor(rate);

        for (let i = 1; i <= ratings; i++) {
            content.push(<FaStar color="#FFEA00" />);
        }
        return content;
    };

    return (
        <div className="flex flex-row border border-[#767676] rounded-2xl p-4 items-center gap-5">
            <div data-cy={`t-image-${index}`}>
                {profileImage ? (
                    <img
                        className="rounded-full w-[86px] h-[86px]"
                        src={profileImage}
                        alt={name}
                    />
                ) : (
                    <div className="uppercase rounded-full h-[60px] w-[60px] lg:w-[86px] lg:h-[86px] bg-gray-200 flex justify-center items-center text-black text-md lg:text-2xl">
                        {getFirstCharacters(name)}
                    </div>
                )}
            </div>

            <div className="flex flex-col gap-4">
                <div className="flex flex-col flex-1">
                    <p
                        className="text-md lg:text-2xl font-semibold"
                        data-cy={`t-name-${index}`}
                    >
                        {name}
                    </p>
                    <p
                        className="text-xs text-[#858585]"
                        data-cy={`t-comment-${index}`}
                    >
                        {comment}
                    </p>
                </div>
                <div
                    className="flex flex-row flex-1"
                    data-cy={`t-rating-${index}`}
                >
                    {renderRate()}
                </div>
            </div>
        </div>
    );
};

export default TestimonyCard;
