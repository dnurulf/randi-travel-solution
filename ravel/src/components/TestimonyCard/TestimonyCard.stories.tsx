/* eslint-disable storybook/story-exports */
import type { Meta, StoryObj } from '@storybook/react';

import { TestimonyCard } from './TestimonyCard.component';

const meta: Meta<typeof TestimonyCard> = {
    component: TestimonyCard,
};

export default meta;
type Story = StoryObj<typeof TestimonyCard>;

const TestimonyCardWithHooks = () => {
    return (
        <div className="flex flex-col gap-10">
            <TestimonyCard
                index={0}
                name="LALA"
                comment="Cool"
                profileImage="https://source.unsplash.com/160x160/?portrait"
                rate={5}
            />
        </div>
    );
};

export const Example: Story = {
    render: () => <TestimonyCardWithHooks />,
};
