export interface IPropTypes {
    name: string;
    comment: string;
    profileImage: string | null;
    rate: number;
    index: number;
}
