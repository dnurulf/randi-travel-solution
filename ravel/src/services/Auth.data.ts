import { axios, AxiosResponse } from '@/utils/axios.utils';

import { IResponse } from '@/types';

import {
    ILoginPayload,
    ILoginResponse,
    IRegisterPayload,
    IRegisterResponse,
} from './Auth.types';

export function login(
    payload: ILoginPayload,
): Promise<AxiosResponse<IResponse<ILoginResponse>>> {
    return axios.post<IResponse<ILoginResponse>>('/api/v1/auam/login', payload);
}

export function register(
    payload: IRegisterPayload,
): Promise<AxiosResponse<IResponse<IRegisterResponse>>> {
    return axios.post<IResponse<IRegisterResponse>>(
        '/api/v1/auam/register',
        payload,
    );
}
