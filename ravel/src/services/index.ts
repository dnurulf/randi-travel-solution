import * as AuthApi from './Auth.data';
import * as ToursApi from './Tours.data';
import * as TestimoniesApi from './Testimony.data';

export { AuthApi, ToursApi, TestimoniesApi };
