export interface IGetTestimonyResponse {
    applicationRate: string;
    image: string | null;
    name: string;
    rateComment: string;
    userId: string;
    _id: string;
}
