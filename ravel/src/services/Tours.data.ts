import { AxiosRequestConfig } from 'axios';
import { axios, AxiosResponse } from '@/utils/axios.utils';

import { IResponse } from '@/types';

import {
    IGetNewTourResponse,
    IGetToursParams,
    IGetToursResponse,
    IGetTourDetailResponse,
} from './Tours.types';

/** Gets a list of new tours */
export function getNewTour(
    config: AxiosRequestConfig,
): Promise<AxiosResponse<IResponse<IGetNewTourResponse>>> {
    return axios.get<IResponse<IGetNewTourResponse>>(
        `/api/v1/tours/new-tour`,
        config,
    );
}

/** Gets a list of top tours */
export function getTopTours(
    config: AxiosRequestConfig,
): Promise<AxiosResponse<IResponse<IGetNewTourResponse[]>>> {
    return axios.get<IResponse<IGetNewTourResponse[]>>(
        `/api/v1/tours/top-5-tour`,
        config,
    );
}

/** Gets a list of searched tours */
export function getTours(
    params: IGetToursParams,
    config: AxiosRequestConfig,
): Promise<AxiosResponse<IResponse<IGetToursResponse[]>>> {
    return axios.get<IResponse<IGetToursResponse[]>>(`/api/v1/tours`, {
        params: params,
        ...config,
    });
}

/** Gets a tour */
export function getDetailTour(
    id: string,
    config: AxiosRequestConfig,
): Promise<AxiosResponse<IResponse<IGetTourDetailResponse>>> {
    return axios.get<IResponse<IGetTourDetailResponse>>(
        `/api/v1/tours/${id}`,
        config,
    );
}
