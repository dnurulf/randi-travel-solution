export interface ILoginPayload {
    userId: string;
    password: string;
}

export interface ILoginResponse {
    name: string;
    accessToken: string;
}

export interface IRegisterPayload {
    userId: string;
    name: string;
    password: string;
}

export interface IRegisterResponse {
    userId: string;
    name: string;
}
