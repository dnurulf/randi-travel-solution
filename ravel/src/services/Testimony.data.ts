import { AxiosRequestConfig } from 'axios';
import { axios, AxiosResponse } from '@/utils/axios.utils';

import { IResponse } from '@/types';

import { IGetTestimonyResponse } from './Testimony.types';

/** Gets a list of testimonies */
export function getTestimonies(
    config: AxiosRequestConfig,
): Promise<AxiosResponse<IResponse<IGetTestimonyResponse[]>>> {
    return axios.get<IResponse<IGetTestimonyResponse[]>>(
        `/api/v1/testimonies`,
        config,
    );
}
