export interface IGetNewTourResponse {
    _id: string;
    description: string;
    image: string;
    name: string;
    price: number;
    rating: number;
    slug: string;
}

export interface IGetTopToursResponse {
    description: string;
    image: string;
    name: string;
    rating: number;
    _id: string;
}

export interface IGetToursParams {
    search: string;
    limit?: number;
}

export interface IGetToursResponse extends IGetNewTourResponse {}

export interface IGetTourDetailResponse extends IGetNewTourResponse {}
