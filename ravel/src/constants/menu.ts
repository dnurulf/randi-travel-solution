import { IMenu } from '@/types';

export const menus: IMenu[] = [
    {
        label: 'Home',
        to: '/',
        dataName: 'h-nav-home',
    },
    {
        label: 'Tempat Lain',
        to: '/pencarian',
        dataName: 'h-nav-other-place',
    },
    {
        label: 'Testimoni',
        to: '/ulasan',
        dataName: 'h-nav-testimoni',
    },
];
