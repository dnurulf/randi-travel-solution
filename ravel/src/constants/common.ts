import { IDropdownOption } from '@/types';

export const profileDropdownOption: IDropdownOption[] = [
    {
        label: 'Logout',
        value: 'Logout',
        dataName: 'btn-logout',
    },
];
