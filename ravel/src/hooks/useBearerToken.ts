import { useAppSelector } from '@/redux/hooks';

export const useBearerToken = () => {
    const { user } = useAppSelector((state) => state.session);
    return {
        headers: {
            Authorization: `Bearer ${user.accessToken}`,
        },
    };
};

export default useBearerToken;
