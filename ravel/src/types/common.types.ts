export interface IMenu {
    label: string;
    to: string;
    dataName: string;
}

export interface IDropdownOption {
    value: string;
    label: string;
    dataName: string;
}
