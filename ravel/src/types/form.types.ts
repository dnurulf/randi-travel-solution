export interface ILoginForm {
    userId: string;
    password: string;
}

export interface IRegisterForm {
    userId: string;
    name: string;
    password: string;
    confirmPassword: string;
}
