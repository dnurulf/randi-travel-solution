import * as Yup from 'yup';

// auth schema
export const loginSchema = Yup.object().shape({
    userId: Yup.string().required('Field ini wajib diisi'),
    password: Yup.string().required('Field ini wajib diisi'),
});

export const registerSchema = Yup.object().shape({
    userId: Yup.string()
        .trim() // Remove leading and trailing spaces
        .matches(
            /^[a-zA-Z0-9 ]*$/,
            'User ID hanya boleh diisi huruf dan angka saja',
        )
        .required('Field ini wajib diisi'),
    name: Yup.string()
        .matches(/^[A-Za-z\s]*$/, 'Nama hanya boleh diisi huruf dan spasi saja')
        .required('Field ini wajib diisi'),
    password: Yup.string().required('Field ini wajib diisi'),
    confirmPassword: Yup.string()
        .oneOf([Yup.ref('password')], 'Password harus sama')
        .required('Field ini wajib diisi'),
});
