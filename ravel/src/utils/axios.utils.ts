import axiosDefault from 'axios';

const axios = axiosDefault.create({
    baseURL: process.env.NEXT_PUBLIC_API_URL,
});

axios.interceptors.response.use(
    (res) => res,
    (err) => {
        if (err?.response.status === 401) {
            console.log(err?.response.status);

            return Promise.reject(err);
        }

        return Promise.reject(err);
    },
);

export { axios };

export type { AxiosResponse } from 'axios';
