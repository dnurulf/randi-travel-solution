/**
 * Truncates a string to a specified length and appends an ellipsis if needed.
 *
 * @param {string} str - The string to truncate.
 * @param {number} maxLength - The maximum length of the truncated string.
 * @returns {string} - The truncated string with an ellipsis if it was truncated.
 */
export function truncate(str: string, maxLength: number): string {
    if (str.length <= maxLength) {
        return str;
    }
    return str.slice(0, maxLength - 3) + '...';
}

/**
 * Formats a number as a currency string in Indonesian Rupiah (IDR).
 *
 * @param {number} amount - The amount to format.
 * @returns {string} - The formatted currency string.
 */
export function formatPrice(amount: number): string {
    return new Intl.NumberFormat('id-ID', {
        style: 'currency',
        currency: 'IDR',
        minimumFractionDigits: 0,
        maximumFractionDigits: 0,
    }).format(amount);
}

/**
 * Processes an input string by replacing spaces with dots, allowing only alphanumeric characters,
 * and ensuring no extra spaces.
 *
 * @param {string} input - The input string to process.
 * @returns {string} - The processed string.
 */
export function processInput(input: string): string {
    // Mengganti spasi dengan titik
    input = input.replace(/\s+/g, '.');

    // Menghapus spasi ekstra (seandainya ada)
    input = input.replace(/\.+/g, '.');

    return input;
}

/**
 * Extracts the first character of every word in a given string, where words are separated by dots.
 *
 * @param {string} input - The input string from which to extract first characters of words.
 * @returns {string[]} An array of the first characters of each word in the input string.
 *
 */
export function getFirstCharacters(input: string): string[] {
    // Split the input string into words using dot as delimiter.
    const words = input.split(' ');

    const firstCharacters = words.map((word) =>
        word.length > 0 ? word[0] : '',
    );

    return firstCharacters;
}
