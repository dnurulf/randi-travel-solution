import { createSlice } from '@reduxjs/toolkit';

export interface Session {
    user: {
        accessToken: string;
        name: string;
    };
}

const initialState: Session = {
    user: {
        accessToken: '',
        name: '',
    },
};

export const sessionSlice = createSlice({
    name: 'session',
    initialState,
    reducers: {
        setSession: (state: Session, action: { payload: any }) =>
            action.payload,
    },
});

export const { setSession } = sessionSlice.actions;

export default sessionSlice.reducer;
