import React from 'react';

import { AppHeader, AppFooter } from '@/components';
import { menus } from '@/constants';

interface IPropTypes {
    children: JSX.Element | string;
}

export function DefaultLayout({ children }: IPropTypes) {
    return (
        <>
            <AppHeader menus={menus} />
            <main>
                <div className="min-h-screen">{children}</div>
            </main>
            <AppFooter />
        </>
    );
}

export default DefaultLayout;
