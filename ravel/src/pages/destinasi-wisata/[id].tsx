/* eslint-disable @next/next/no-img-element */
import React, { ReactElement } from 'react';
import { useQuery } from '@tanstack/react-query';
import { useParams } from 'next/navigation';

import { DefaultLayout } from '@/layouts';
import { useBearerToken } from '@/hooks';

import { formatPrice } from '@/utils';
import { ToursApi } from '@/services';

import type { NextPageWithLayout } from '../_app';

export const DetailDestination: NextPageWithLayout = () => {
    // hooks
    const accessToken = useBearerToken();
    const params = useParams<{ id: string }>();

    // request
    const {
        data: tour,
        isLoading,
        isSuccess,
    } = useQuery({
        queryKey: ['get-detail-tour', params],
        queryFn: async () => {
            const result = await ToursApi.getDetailTour(params.id, accessToken);

            return result.data.data;
        },
    });

    return (
        <section className="flex flex-col items-center py-9 lg:gap-16 gap-8">
            {isLoading && <p className="text-xl">Loading...</p>}

            {isSuccess && (
                <>
                    <div className="flex flex-col items-center">
                        <p
                            className="text-5xl text-green-400 font-bold"
                            data-cy="tour-title"
                        >
                            {tour.name}
                        </p>
                        <p
                            className="text-4xl font-semibold"
                            data-cy="tour-slug"
                        >
                            {tour.slug}
                        </p>
                    </div>

                    <div className="h-[380px] w-full flex">
                        <img
                            data-cy="tour-image"
                            src={tour.image}
                            alt="Surabaya"
                            className="object-cover object-center"
                        />
                    </div>

                    <div className="flex flex-col items-center gap-6 text-center w-[300px] lg:w-[600px]">
                        <div className="flex flex-row">
                            <p className="font-semibold text-md">{tour.name}</p>
                            ,{' '}
                            <p className="font-semibold text-md text-green-500">
                                {tour.slug}
                            </p>
                        </div>

                        <p className="text-extralight" data-cy="tour-desc">
                            {tour.description}
                        </p>

                        <p className="font-bold" data-cy="tour-price">
                            Mulai dari: {formatPrice(tour.price)}
                        </p>
                    </div>
                </>
            )}
        </section>
    );
};

DetailDestination.getLayout = function getLayout(page: ReactElement) {
    return <DefaultLayout>{page}</DefaultLayout>;
};

export default DetailDestination;
