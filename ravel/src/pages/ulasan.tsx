/* eslint-disable @next/next/no-img-element */
import React, { ReactElement, useState } from 'react';
import { useRouter } from 'next/router';
import { useQuery } from '@tanstack/react-query';

import { DefaultLayout } from '@/layouts';
import {
    PageTitle,
    SearchInput,
    ProductItem,
    TestimonyCard,
} from '@/components';
import { TestimoniesApi } from '@/services';
import { useBearerToken } from '@/hooks';

import type { NextPageWithLayout } from './_app';

export const DetailDestination: NextPageWithLayout = () => {
    // hooks
    const accessToken = useBearerToken();

    // request
    const {
        data: testimonies,
        isLoading,
        isSuccess,
    } = useQuery({
        queryKey: ['get-testimonies'],
        queryFn: async () => {
            const result = await TestimoniesApi.getTestimonies(accessToken);

            return result.data.data;
        },
    });

    return (
        <section className="flex flex-col px-5 lg:px-14 py-10 lg:py-9 lg:gap-20 gap-5">
            <div className="px-20">
                <p
                    className="text-xl lg:text-5xl font-bold text-center"
                    data-cy="testimony-header"
                >
                    Semua <span className="text-green-400">Testimoni</span> Kami
                </p>
            </div>

            {isLoading && <p className="text-lg">Loading...</p>}

            {isSuccess && (
                <div className="grid lg:grid-cols-2 grid-cols-1 grid-flow-row gap-10">
                    {testimonies.map((item, index) => (
                        <div key={item._id}>
                            <TestimonyCard
                                index={index}
                                name={item.name}
                                comment={item.rateComment}
                                profileImage={item.image}
                                rate={5}
                            />
                        </div>
                    ))}
                </div>
            )}
        </section>
    );
};

DetailDestination.getLayout = function getLayout(page: ReactElement) {
    return <DefaultLayout>{page}</DefaultLayout>;
};

export default DetailDestination;
