/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';
import { useRouter } from 'next/router';

import { Card, Input, PasswordInput, Button, Toast } from '@/components';
import { IRegisterForm } from '@/types';
import { registerSchema, processInput } from '@/utils';
import { IRegisterPayload } from '@/services/Auth.types';
import { AuthApi } from '@/services';

export const Login = (): JSX.Element => {
    // hooks
    const router = useRouter();
    const {
        handleSubmit,
        control,
        formState: { errors },
    } = useForm<IRegisterForm>({
        resolver: yupResolver(registerSchema),
        defaultValues: {
            userId: '',
            name: '',
            password: '',
            confirmPassword: '',
        },
    });

    // state
    const [messageRequest, setMessageRequest] = useState('');

    // requests
    const { mutate, isPending, isError } = useMutation({
        mutationFn: (payload: IRegisterPayload) => AuthApi.register(payload),
        onSuccess: (res) => {
            setMessageRequest(
                `Hi, ${res.data.data.name}, Anda telah terdaftar!`,
            );

            setTimeout(() => {
                router.push('/login');
            }, 1000);
        },
        onError: (error: Record<string, any>) => {
            setMessageRequest(
                error.response.data.error || error.response.data.message,
            );
        },
    });

    // methods
    const onRegister = async (values: IRegisterForm) => {
        setMessageRequest('');
        console.log({
            ...values,
            userId: processInput(values.userId),
        });

        await mutate({
            userId: processInput(values.userId),
            password: values.password,
            name: values.name,
        });
    };

    return (
        <div className="flex flex-row h-screen bg-register-background bg-no-repeat bg-contain bg-center">
            <div className="flex-1 flex justify-center items-center">
                <Card>
                    <div className="flex flex-col w-[453px] gap-7">
                        <p className="text-2xl font-bold">Daftar</p>

                        <form
                            onSubmit={handleSubmit(onRegister)}
                            className="flex flex-col gap-4"
                        >
                            <Input
                                data-cy="input-userId"
                                control={control}
                                label="User ID"
                                name="userId"
                                placeholder="Masukkan User ID"
                                errors={errors.userId?.message}
                            />

                            <Input
                                data-cy="input-name"
                                control={control}
                                label="Nama"
                                name="name"
                                placeholder="Masukkan Nama"
                                errors={errors.name?.message}
                            />

                            <PasswordInput
                                data-cy="input-password"
                                control={control}
                                label="Password"
                                name="password"
                                placeholder="Masukkan Password"
                                errors={errors.password?.message}
                            />

                            <PasswordInput
                                data-cy="input-confirmation_password"
                                control={control}
                                label="Konfirmasi"
                                name="confirmPassword"
                                placeholder="Masukkan Konfirmasi Password"
                                errors={errors.confirmPassword?.message}
                            />

                            <div className="mt-6" />

                            <Button type="submit" isLoading={isPending}>
                                Daftar
                            </Button>
                        </form>

                        {messageRequest && (
                            <Toast
                                type={isError ? 'error' : 'success'}
                                message={messageRequest}
                            />
                        )}
                    </div>
                </Card>
            </div>
        </div>
    );
};

export default Login;
