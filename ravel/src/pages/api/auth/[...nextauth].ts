import NextAuth from 'next-auth';
import CredentialsProvider from 'next-auth/providers/credentials';

import { AuthApi } from '@/services';

export default NextAuth({
    providers: [
        CredentialsProvider({
            name: 'Credentials',
            credentials: {
                userId: { label: 'User ID', type: 'text' },
                password: { label: 'Password', type: 'password' },
            },
            async authorize(credentials) {
                console.log(credentials);
                const result: any = await AuthApi.login({
                    userId: String(credentials?.userId),
                    password: String(credentials?.password),
                });

                const { data, code } = result.data;

                if (code === 200) {
                    return {
                        ...data,
                    };
                } else {
                    throw new Error(result?.data?.message?.text_en);
                }
            },
        }),
    ],
    callbacks: {
        jwt: ({ token, user }: Record<string, any>) => {
            if (user) {
                token.id = user.id;
                token.profiles = user.profiles;
                token.access_token = user.token;
                token.company = user.company;
            }

            return token;
        },
        session: async ({ session, user, token }: Record<string, any>) => {
            if (token) {
                session.id = token.id;
                session.token = token.access_token;
                session.profiles = token.profiles;
                session.company = token.company;
            }

            return session;
        },
    },
    pages: {
        signIn: '/',
        signOut: '/login',
        error: '/login',
    },
    secret: process.env.JWT_SECRET,
});
