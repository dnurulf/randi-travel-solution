/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { useState } from 'react';
import Link from 'next/link';
import { useRouter } from 'next/router';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { useMutation } from '@tanstack/react-query';

import { useAppDispatch } from '@/redux/hooks';
import {
    Card,
    Input,
    PasswordInput,
    Checkbox,
    Button,
    Toast,
} from '@/components';
import { loginSchema } from '@/utils';
import { ILoginForm } from '@/types';
import { AuthApi } from '@/services';
import { setSession } from '@/redux/reducer/session';

export const Login = (): JSX.Element => {
    // hooks
    const dispatch = useAppDispatch();
    const router = useRouter();
    const {
        handleSubmit,
        control,
        formState: { errors },
    } = useForm<ILoginForm>({
        resolver: yupResolver(loginSchema),
        defaultValues: {
            userId: '',
            password: '',
        },
    });

    // state
    const [errorMessageLogin, setErrorMessageLogin] = useState('');

    // requests
    const { mutate, isPending } = useMutation({
        mutationFn: (payload: ILoginForm) => AuthApi.login(payload),
        onSuccess: (res) => {
            document.cookie = `token=${res.data.data.accessToken}; path=/;`;
            localStorage.setItem('userInfo', res.data.data.name);
            localStorage.setItem('authToken', res.data.data.accessToken);
            localStorage.setItem('justLoggedIn', 'true');

            dispatch(
                setSession({
                    user: {
                        accessToken: res.data.data.accessToken,
                        name: res.data.data.name,
                    },
                }),
            );

            router.push('/');
        },
        onError: (error: Record<string, any>) => {
            setErrorMessageLogin(
                error.response.data.error || error.response.data.message,
            );
        },
    });

    // methods
    const onLogin = async (values: ILoginForm) => {
        setErrorMessageLogin('');
        await mutate({ ...values });
    };

    return (
        <div className="flex flex-row h-screen">
            <div className="flex-1 bg-login-background bg-no-repeat bg-cover hidden lg:block"></div>
            <div className="flex-1 flex justify-center items-center">
                <Card>
                    <div className="flex flex-col w-[453px] gap-4">
                        <img
                            className="m-auto"
                            src="Logo.png"
                            alt="Logo"
                            width={300}
                            height={100}
                        />

                        <p className="text-lg font-bold">Masuk</p>

                        <form
                            className="flex flex-col gap-4"
                            onSubmit={handleSubmit(onLogin)}
                        >
                            <Input
                                data-cy="input-userId"
                                control={control}
                                label="User ID"
                                name="userId"
                                placeholder="Masukkan User ID"
                                errors={errors.userId?.message}
                            />

                            <PasswordInput
                                data-cy="input-password"
                                control={control}
                                label="Password"
                                name="password"
                                placeholder="Masukkan Password"
                                errors={errors.password?.message}
                            />

                            <Checkbox
                                data-cy="input-remember_me"
                                control={control}
                                label="Ingat Saya"
                                name="rememberMe"
                            />

                            <Button
                                type="submit"
                                data-cy="btn-login"
                                isLoading={isPending}
                            >
                                Login
                            </Button>
                        </form>

                        <div className=" flex flex-col gap-3 text-center">
                            <p className="font-semibold">atau</p>
                            <p className="text-sm text-font-300">
                                Belum punya akun?{' '}
                                <Link
                                    data-cy="btn-register"
                                    className="text-green-500 font-bold"
                                    href="/register"
                                >
                                    Daftar
                                </Link>
                            </p>
                        </div>
                    </div>
                </Card>

                {errorMessageLogin && (
                    <Toast type="error" message={errorMessageLogin} />
                )}
            </div>
        </div>
    );
};

export default Login;
