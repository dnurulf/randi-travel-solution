/* eslint-disable @next/next/no-img-element */
import React, { ReactElement, useState } from 'react';
import { useQuery } from '@tanstack/react-query';
import debounce from 'lodash/debounce';
import Link from 'next/link';

import { DefaultLayout } from '@/layouts';
import { PageTitle, SearchInput, ProductItem } from '@/components';
import { useAppSelector } from '@/redux/hooks';
import { useBearerToken } from '@/hooks';
import { ToursApi } from '@/services';

import type { NextPageWithLayout } from './_app';
import { IGetToursResponse } from '@/services/Tours.types';

const renderTitleTour = (title = '', slug = '', index: number): JSX.Element => {
    return (
        <>
            <span data-cy={`tour-title-${index}`}>{title}</span>
            {', '}
            <span
                data-cy={`tour-slug-${index}`}
                className="text-green-500 font-bold"
            >
                {slug}
            </span>
        </>
    );
};

const renderTitle = (title = '', slug = ''): JSX.Element => {
    return (
        <>
            {title} <span className="text-green-500 font-bold">{slug}</span>
        </>
    );
};

export const Explore: NextPageWithLayout = () => {
    // hooks
    const accessToken = useBearerToken();
    const { user } = useAppSelector((state) => state.session);

    // state
    const [keyword, setKeyword] = useState('');

    // requests
    const {
        data: tours,
        isLoading,
        isSuccess,
    } = useQuery({
        queryKey: ['get-tours', keyword],
        queryFn: async () => {
            const result = await ToursApi.getTours(
                { search: keyword, limit: 2 },
                accessToken,
            );

            return result.data.data;
        },
        enabled: keyword.length > 0,
    });

    // methods
    const handleSearch = (value: string) => {
        setKeyword(value);
    };

    const debouncedChangeHandler = debounce(handleSearch, 300);

    return (
        <section className="flex flex-col px-6 py-10 gap-5 lg:px-20 lg:py-9 lg:gap-20">
            <div className="flex flex-col items-center gap-9">
                <div className="flex-1">
                    <PageTitle
                        dataName="h-search-page"
                        title={renderTitle('Halo', user.name)}
                        subtitle="Kemana Kau Ingin Pergi?"
                    />
                </div>
                <div className="flex-1 w-[380px] lg:w-[520px]">
                    <SearchInput
                        data-cy="input-search-tour"
                        onChange={debouncedChangeHandler}
                    />
                </div>
            </div>

            <div>
                {isLoading && <p className="text-lg">Loading...</p>}

                {isSuccess && (
                    <>
                        <p className="font-semibold mb-4 text-lg text-center lg:text-left">
                            Hasil Pencarian mu:{' '}
                        </p>
                        <div className="flex flex-col">
                            {tours && tours.length ? (
                                tours.map(
                                    (
                                        item: IGetToursResponse,
                                        index: number,
                                    ) => (
                                        <Link
                                            href={`/destinasi-wisata/${item._id}`}
                                            key={item._id}
                                        >
                                            <ProductItem
                                                index={index}
                                                title={renderTitleTour(
                                                    item.name,
                                                    item.slug,
                                                    index,
                                                )}
                                                description={item.description}
                                                image={item.image}
                                                price={item.price}
                                            />

                                            {index < tours.length - 1 && (
                                                <hr className="my-6" />
                                            )}
                                        </Link>
                                    ),
                                )
                            ) : (
                                <p>
                                    Hasil pencarian tidak ditemukan. Silakan
                                    ganti kata kunci!
                                </p>
                            )}
                        </div>
                    </>
                )}

                {tours && tours.length ? (
                    <p className="text-center my-12 font-semibold">
                        Akhir dari pencarian...
                    </p>
                ) : (
                    false
                )}
            </div>
        </section>
    );
};

Explore.getLayout = function getLayout(page: ReactElement) {
    return <DefaultLayout>{page}</DefaultLayout>;
};

export default Explore;
