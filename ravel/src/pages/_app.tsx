/* eslint-disable react-hooks/exhaustive-deps */
import React, { ReactElement, ReactNode, useEffect } from 'react';
import type { AppProps } from 'next/app';
import { NextPage } from 'next/types';
import { QueryClient, QueryClientProvider } from '@tanstack/react-query';
import { SessionProvider } from 'next-auth/react';
import { Provider } from 'react-redux';
import { useRouter } from 'next/router';

import { store } from '@/redux/store';

import '@/styles/globals.css';
import '@/styles/form.css';

export type NextPageWithLayout<P = {}, IP = P> = NextPage<P, IP> & {
    getLayout?: (page: ReactElement) => ReactNode;
};

type AppPropsWithLayout = AppProps & {
    Component: NextPageWithLayout;
};

export default function App({
    Component,
    pageProps: { session, ...pageProps },
}: AppPropsWithLayout) {
    // Use the layout defined at the page level, if available
    const getLayout = Component.getLayout ?? ((page) => page);
    const router = useRouter();

    const [queryClient] = React.useState(
        () =>
            new QueryClient({
                defaultOptions: {
                    queries: {
                        staleTime: 60 * 1000,
                    },
                },
            }),
    );

    useEffect(() => {
        const token = localStorage.getItem('authToken');

        if (!token) {
            if (router.pathname === '/register') {
                router.push('/register');
            } else {
                router.push('/login');
            }
        } else {
            router.push('/');
        }
    }, []);

    return (
        <Provider store={store}>
            <QueryClientProvider client={queryClient}>
                <SessionProvider session={session}>
                    {getLayout(<Component {...pageProps} />)}
                </SessionProvider>
            </QueryClientProvider>
        </Provider>
    );
}
