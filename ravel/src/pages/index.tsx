/* eslint-disable react-hooks/exhaustive-deps */
/* eslint-disable @next/next/no-img-element */
import React, { ReactElement, useEffect, useState } from 'react';
import { useRouter } from 'next/router';
import { useQuery } from '@tanstack/react-query';
import Link from 'next/link';

import { DefaultLayout } from '@/layouts';
import {
    HeaderImage,
    HeaderNewTour,
    ProductCard,
    Toast,
    TestimonySection,
} from '@/components';
import { IGetTopToursResponse } from '@/services/Tours.types';
import { setSession } from '@/redux/reducer/session';
import { useAppSelector, useAppDispatch } from '@/redux/hooks';
import { ToursApi } from '@/services';
import { useBearerToken } from '@/hooks';

import type { NextPageWithLayout } from './_app';
import { truncate } from '@/utils';

const renderTitle = (title = '', slug = ''): JSX.Element => {
    return (
        <>
            {title} <span className="text-green-500 font-bold">{slug}</span>!
        </>
    );
};

const renderSubtitle = (title = '', slug = ''): JSX.Element => {
    return (
        <>
            {title}, <span className="text-green-500 font-bold">{slug}</span>
        </>
    );
};

export const LandingPage: NextPageWithLayout = () => {
    // hoooks
    const accessToken = useBearerToken();
    const router = useRouter();
    const dispatch = useAppDispatch();
    const { user } = useAppSelector((state) => state.session);

    const [showToast, setShowToast] = useState(false);

    // requests
    const { data: newTour } = useQuery({
        queryKey: ['get-new-tour', accessToken],
        queryFn: async () => {
            const result = await ToursApi.getNewTour(accessToken);

            return result.data.data;
        },
        enabled: !!accessToken,
    });

    const { data: topTours } = useQuery({
        queryKey: ['get-top-five-tour', accessToken],
        queryFn: async () => {
            const result = await ToursApi.getTopTours(accessToken);

            return result.data.data;
        },
        enabled: !!accessToken,
    });

    // methods
    const handleRedirect = () => {
        router.push('/ulasan');
    };

    // cycles
    useEffect(() => {
        if (localStorage.getItem('justLoggedIn')) {
            setShowToast(true);

            localStorage.removeItem('justLoggedIn'); // Clear the flag

            setTimeout(() => {
                setShowToast(false);
            }, 2000);
        }

        if (localStorage.getItem('authToken')) {
            dispatch(
                setSession({
                    user: {
                        accessToken: localStorage.getItem('authToken'),
                        name: localStorage.getItem('userInfo'),
                    },
                }),
            );
        }
    }, []);

    return (
        <div>
            <HeaderImage />
            <div>
                <section className="px-10 py-10 sm:px-20 sm:py-14">
                    {newTour && (
                        <HeaderNewTour
                            title={renderTitle('Tujuan', 'Baru')}
                            subtitle={renderSubtitle(
                                String(newTour?.name),
                                String(newTour?.slug),
                            )}
                            image={String(newTour?.image)}
                        >
                            {String(newTour?.description)}
                        </HeaderNewTour>
                    )}
                </section>

                <section
                    data-cy="top-5-tour-container"
                    className="flex flex-col bg-[#FAFAFA] py-16 gap-16"
                >
                    <p className="text-2xl font-bold text-center">
                        <span className="text-green-500">Explore</span> Tempat
                        Lainnya
                    </p>

                    <div className="grid grid-cols-1 md:grid-cols-3 gap-4 lg:grid-cols-5">
                        {topTours &&
                            topTours.map(
                                (
                                    item: IGetTopToursResponse,
                                    index,
                                ): JSX.Element => (
                                    <Link
                                        href={`/destinasi-wisata/${item._id}`}
                                        date-cy={`header-top-tour-${index}`}
                                        key={item._id}
                                        className="m-auto"
                                    >
                                        <ProductCard
                                            title={item.name}
                                            description={truncate(
                                                item.description,
                                                50,
                                            )}
                                            image={item.image}
                                        />
                                    </Link>
                                ),
                            )}
                    </div>
                </section>

                <section className="py-14 px-20">
                    <TestimonySection
                        title={renderTitle('Experience Nomor', 'Satu')}
                        subtitle={'Dipercaya Seluruh Dunia'}
                        image="/SectionItemImage.png"
                        onCallToActionClick={handleRedirect}
                    >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                        Pellentesque dui risus, consectetur quis est quis,
                        ornare varius est. Proin viverra euismod lorem, sed
                        congue sem rhoncus ut. Donec eget euismod mauris,
                        hendrerit rhoncus ipsum. Nam eleifend semper massa sed
                        rhoncus. Aliquam erat volutpat. Donec nec dignissim
                        turpis, sed ultrices nisi. Duis ligula velit, feugiat
                        quis varius vitae, fringilla ut nisi. Donec feugiat
                        porta nulla quis maximus. Maecenas consequat ac nunc at
                        commodo. Phasellus efficitur maximus nunc quis placerat.
                        Sed vitae tempor risus. Sed ornare sagittis rutrum.
                        Mauris mattis eleifend enim vel gravida. Nam quis
                        vestibulum magna. Pellentesque mattis, neque non
                        volutpat dignissim, risus mauris consectetur diam,
                        faucibus accumsan ligula lectus eu ante. Fusce semper
                        auctor lorem, sit amet feugiat lacus scelerisque sed.
                        Donec a justo dui. Aenean tristique viverra nunc.
                        Pellentesque ut viverra mauris.
                    </TestimonySection>
                </section>

                {showToast && (
                    <Toast
                        type="success"
                        message={`Selamat Datang, ${user.name} `}
                    />
                )}
            </div>
        </div>
    );
};

LandingPage.getLayout = function getLayout(page: ReactElement) {
    return <DefaultLayout>{page}</DefaultLayout>;
};

export default LandingPage;
