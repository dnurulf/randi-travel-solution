import type { Config } from 'tailwindcss';

const config: Config = {
    content: [
        './src/pages/**/*.{js,ts,jsx,tsx,mdx}',
        './src/components/**/*.{js,ts,jsx,tsx,mdx}',
        './src/app/**/*.{js,ts,jsx,tsx,mdx}',
    ],
    theme: {
        extend: {
            colors: {
                thunderbird: {
                    50: '#FFF1F1',
                    800: '#A70D0E',
                    900: '#8c0b0b',
                },
                font: {
                    50: '#FAFAFA',
                    100: '#F4F4F5',
                    200: '#E4E4E7',
                    300: '#CCCCCC',
                    500: '#70707B',
                    800: '#26272B',
                },
                navBorder: '#E3E3E3',
                sidebar: '#F8F8F8',
            },
            fontFamily: {
                poppins: ['Poppins'],
            },
            backgroundImage: {
                'login-background': "url('/LoginImage.jpg')",
                'register-background': "url('/RegisterBackground.png')",
            },
        },
    },
    plugins: [],
};
export default config;
